import Vue from 'vue';
import Vuetify from 'vuetify';
import 'vuetify/dist/vuetify.min.css';
import fr from 'vuetify/es5/locale/fr';

Vue.use(Vuetify);

export default new Vuetify({
  theme: {
    options: {
      customProperties: true,
    },
    themes: {
      light: {
        primary: '#82B1FF',
        secondary: '#4CAF50',
        accent: '#82B1FF',
        error: '#FF5252',
        info: '#a9c23c',
        success: '#4CAF50',
        warning: '#FFC107'
      },
      dark: {
        primary: '#91337e',
      
      },
    },
  },
  lang: {
    locales: { fr },
    current: 'fr',
  },
  icons: {
    iconfont: 'fa',
  },
});
